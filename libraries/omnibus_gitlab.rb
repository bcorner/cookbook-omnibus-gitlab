module OmnibusGitlab
  def self.fetch_from_databag(node, *path)
    node_attributes = GitLab::Vault.fetch_path(node, path) # eg: node['omnibus-gitlab']['gitlab_rb']
    databag_secrets = environment_secrets_for_node(node) # eg: {"omnibus-gitlab": {"gitlab_rb": {}}} OR {}

    if databag_secrets.any?
      secrets = fetch_or_init(databag_secrets, path)
      Chef::Mixin::DeepMerge.deep_merge(secrets.to_hash, node_attributes.to_hash)
    else
      node_attributes
    end
  end

  def self.environment_secrets_for_node(node)
    data_bag_name = node['omnibus-gitlab']['data_bag']
    data_bag_element = node.chef_environment

    if data_bag_name && Chef::Search::Query.new.search(data_bag_name, "id:#{data_bag_element}").any?
      data_bag_item(data_bag_name, data_bag_element).to_hash
    else
      {}
    end
  end

  def self.fetch_or_init(hash, path)
    result = hash

    path.each do |p|
      result[p] ||= {}
      result = result[p]
    end

    result
  end
end
